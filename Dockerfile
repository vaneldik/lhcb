FROM gitlab-registry.cern.ch/etf/docker/etf-exp:qa

LABEL maintainer="Marian Babik <Marian.Babik@cern.ch>"
LABEL description="WLCG ETF LHCb"
LABEL version="1.0"

ENV NSTREAM_ENABLED=0

# Middleware
RUN yum -y install yum-priorities
# RUN rpm -ivh http://repository.egi.eu/sw/production/umd/4/centos7/x86_64/updates/umd-release-4.1.3-1.el7.centos.noarch.rpm
# RUN rpm -import http://repository.egi.eu/sw/production/umd/UMD-RPM-PGP-KEY
# RUN cd /etc/yum.repos.d/ && wget https://research.cs.wisc.edu/htcondor/yum/repo.d/htcondor-stable-rhel7.repo
# COPY ./config/htcondor_stable.repo /etc/yum.repos.d/htcondor-stable-rhel7.repo
# COPY ./config/storage_ci.repo /etc/yum.repos.d/storage_ci.repo

# Core
RUN yum -y install voms voms-clients-java

# CONDOR
RUN yum -y install --nogpgcheck condor condor-python 

# ARC
RUN rpm -ivh https://download.nordugrid.org/packages/nordugrid-release/releases/6/centos/el7/x86_64/nordugrid-release-6-1.el7.noarch.rpm
RUN yum -y install nordugrid-arc-client nordugrid-arc-plugins-needed nordugrid-arc-plugins-globus

# Xroot
RUN yum -y install xrootd-python xrootd-client xrootd-libs xrootd-client-libs gfal2-plugin-xrootd

# SRM
RUN yum -y install python2-gfal2 python2-gfal2-util gfal2-plugin-srm gfal2-plugin-gridftp gfal2-plugin-http gfal2-plugin-sftp

# HTTP/webdav
RUN yum -y install python3-gfal2 python3-nap python36-pyOpenSSL

# ETF Plugins
RUN yum -y install python-pip
RUN pip install pexpect 'ptyprocess==0.6.0' argparse
RUN yum -y install --nogpgcheck nagios-plugins-globus nagios-plugins nagios-plugins-webdav python-jess python-wnfm

# ETF WN-qFM payload
RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/bin
RUN cp /usr/bin/etf_wnfm /usr/libexec/grid-monitoring/wnfm/bin/
RUN cp -r /usr/lib/python2.7/site-packages/pexpect /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp -r /usr/lib/python2.7/site-packages/ptyprocess /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp -r /usr/lib/python2.7/site-packages/wnfm /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp /usr/lib/python2.7/site-packages/argparse.py /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages/

# Streaming
RUN mkdir -p /var/spool/nstream/outgoing && chmod 777 /var/spool/nstream/outgoing
RUN mkdir /etc/stompclt
COPY ./config/ocsp_handler.cfg /etc/nstream/

# ARC config
RUN mkdir /opt/omd/sites/$CHECK_MK_SITE/.arc
COPY ./config/client.conf /opt/omd/sites/$CHECK_MK_SITE/.arc/
RUN chown -R $CHECK_MK_SITE /opt/omd/sites/$CHECK_MK_SITE/.arc/

# MW env
COPY ./config/grid-env.sh /etc/profile.d/
RUN echo "source /etc/profile.d/grid-env.sh" >> /opt/omd/sites/$CHECK_MK_SITE/.profile

# LHCb config and payload
COPY ./config/lhcb_vofeed.py /usr/lib/ncgx/x_plugins/
COPY ./config/wlcg_lhcb.cfg /etc/ncgx/metrics.d/
RUN mkdir -p /usr/libexec/grid-monitoring/probes/org.lhcb/wnjob
COPY ./src/probes/WN* /usr/libexec/grid-monitoring/probes/org.lhcb/wnjob/org.lhcb/probes/
COPY ./src/probes/se* /usr/libexec/grid-monitoring/probes/org.lhcb/
COPY ./src/check_pilot_results /usr/libexec/grid-monitoring/probes/org.lhcb/

# ETF config
COPY ./config/lhcb_checks.cfg /etc/ncgx/conf.d/
COPY ./config/ncgx.cfg /etc/ncgx/

EXPOSE 80 443 6557
COPY ./docker-entrypoint.sh /
ENTRYPOINT /docker-entrypoint.sh
