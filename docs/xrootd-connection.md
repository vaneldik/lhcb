# org.lhcb.xrootd-connection
This test performs the following operations:
* Prints the versions of Python, GFAL2, python3-gfal2 and the xrootd client
* Decodes the X.509 proxy certificate, printing its subject, validity checking for the VOMS extensions
* Collects all IP addresses for the endpoint
* Prints the hostnames for each IP address
* Opens a socket connection to each IP address

These events determine a not-OK test result:
| Condition | Test status |
|-----------|-------------|
| Command line argument-related errors | UNKNOWN |
| Expired X.509 proxy | UNKNOWN |
| No VOMS extensions found | WARNING |
| Proxy decoding failed | WARNING |
| No IP addresses found | CRITICAL |
| No name associated with IP | WARNING |
| Connection to an IP timed out | WARNING |
| Connection attempt to an IP failed | WARNING |
| Required to check via IPv4 but no IPv4 address found | CRITICAL |
| Required to check via IPv6 but no IPv6 address found | CRITICAL |
| Required to check via both IPv4 and IPv6 but either IP address not found | WARNING |
| No good IP addresses survived after removing those timing out or unable to connect to | CRITICAL |
