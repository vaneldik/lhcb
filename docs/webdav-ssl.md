# org.lhcb.SE-WebDAV-ssl
This test performs the following operations:
* Fetches and decodes the endpoint server certificate and prints information about it
* Opens a connection to the endpoint via SSL

These events determine a not-OK test result:
| Condition | Test status |
|-----------|-------------|
| Certificate fetching/decoding failed | CRITICAL |
| Expired server certificate | CRITICAL |
| Server certificate expiring in less than a week | WARNING |
| Server certificate check failed | CRITICAL |
| Server certificate validation failed | CRITICAL |
| SSL connection failed | CRITICAL |
| Any other connection failure | CRITICAL |

## Summary
The test will pass only if it is possible to open an SSL connection to the endpoint.
