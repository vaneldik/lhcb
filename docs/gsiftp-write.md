# org.lhcb.SE-GSIftp-write
This test performs the following operations:
* Opens a file for writing on the endpoint via GFAL2
* Gets the file checksum from GFAL2
* Deletes the file from the endpoint via GFAL2

These events determine a not-OK test result:
| Condition | Test status |
|-----------|-------------|
| Number of written bytes differs from buffer size | CRITICAL |
| Size of written file differs from buffer size | CRITICAL |
| GFAL2 produces an error when writing | CRITICAL |
| The file checksum from GFAL2 is incorrect | CRITICAL |
| The file could not be removed using GFAL2 | WARNING |

## Summary
The test will pass if a test file can be correctly written via GFAL2 using the GSIFTP protocol.
