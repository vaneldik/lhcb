import re
import logging
import json
import random

from ncgx.inventory import Hosts, Checks, Groups
from vofeed.api import VOFeed

log = logging.getLogger('ncgx')

FLAVOR_MAP = {'ARC-CE': 'arc',
              'HTCONDOR-CE': 'condor'}

WN_METRICS = {
    'WN-lhcb-env': 'org.lhcb.WN-env-/lhcb/Role=production',
    'WN-lhcb-cvmfs': 'org.lhcb.WN-cvmfs-/lhcb/Role=production',
    'WN-singularity': 'org.lhcb.WN-singularity-/lhcb/Role=production',
}

ARC_METRICS = (
    'org.sam.ARC-JobSubmit-/lhcb/Role=production',
)

CONDOR_METRICS = (
    'org.sam.CONDOR-JobSubmit-/lhcb/Role=production',
)

WEBDAV_METRICS = (
    'org.lhcb.SE-WebDAV-connection',
    'org.lhcb.SE-WebDAV-ssl',
    'org.lhcb.SE-WebDAV-extensions',
    'org.lhcb.SE-WebDAV-write')

GRIDFTP_METRICS = (
    'org.lhcb.SE-GSIftp-connection',
    'org.lhcb.SE-GSIftp-write')

XROOTD_METRICS = (
    'org.lhcb.SE-xrootd-connection',
    #'org.lhcb.SE-xrootd-write')
    )

def run(url, ipv6=False):
    log.info("Processing vo feed: %s" % url)

    # Get services from the VO feed, i.e
    # list of tuples (hostname, flavor, endpoint)
    feed = VOFeed(url)
    services = feed.get_services()

    # Add hosts, each tagged with corresponding flavors
    # creates /etc/ncgx/conf.d/generated_hosts.cfg
    h = Hosts()
    for service in services:
        h.add(service[0], tags=[service[1]])
    h.serialize()

    # Add host groups
    sites = feed.get_groups("LHCb_Site")
    hg = Groups("host_groups")
    for site, hosts in sites.items():
        for host in hosts:
            hg.add(site, host)
    hg.serialize()

    # Add corresponding metrics to tags
    # creates /etc/ncgx/conf.d/generated_checks.cfg
    c = Checks()
    c.add_all(ARC_METRICS, tags=["ARC-CE", ])
    c.add_all(CONDOR_METRICS, tags=["HTCONDOR-CE",
                                    "org.opensciencegrid.htcondorce"])
    c.add_all(WN_METRICS.values(), tags=["ARC-CE", "HTCONDOR-CE",
                                         "org.opensciencegrid.htcondorce"])
    c.add_all(WEBDAV_METRICS, tags=["HTTP"])
    c.add_all(GRIDFTP_METRICS, tags=["GRIDFTP"])
    c.add_all(XROOTD_METRICS, tags=["XROOTD"])
    c.add("org.lhcb.DNS-IPv6", tags=["SRMv2"], params={'extends': 'check_dig'})

    # ETF env - environment variables to export on the worker node
    # ETF_TESTS pointing to a list of WN tests to execute
    with open('/tmp/etf-env.sh', 'w') as etf_env:
        etf_env.write('ETF_TESTS={}\n'.format(','.join(['etf/probes/'+m for m in WN_METRICS.keys()])))

    # ETF WN-qFM config - maps WN tests to metrics (WN-cvmfs -> org.lhcb.WN-cvmfs-/lhcb/Role=production)
    with open('/tmp/etf_wnfm.json', 'w') as etf_wnfm:
        json.dump({'wn_metric_map': WN_METRICS, 'counter_enabled': True}, etf_wnfm)

    # Queues
    for service in services:
        host = service[0]
        flavor = service[1]
        endpoint = service[2]
        if flavor not in ["ARC-CE", "HTCONDOR-CE"]:
            continue
        ce_resources = feed.get_queues(host, flavor)
        if ce_resources:
            if len(ce_resources) > 1:
                log.warning("More than one queue found for service %s (%s) - %s" % (host, flavor, ce_resources))
                ce_res_nogpu = [e for e in ce_resources if
                                all(sp not in e[1].lower() for sp in ['gpu',
                                            'debug', 'atlas', 'cms', 'alice',
                                            'himem'])]
                if ce_res_nogpu:
                    # pick at random from remaining queues
                    ce_res = random.choice(ce_res_nogpu)
                    queue = ce_res[1]
                else:
                    log.error("Only restricted queues found for service %s (%s), skipping" % (host, flavor))
                    continue
            else:
                # just one queue option
                queue = ce_resources[0][1]
            if flavor not in FLAVOR_MAP.keys():
                log.error("Unable to determine type for flavour %s" % flavor)
                continue
            res = "%s://%s/%s/%s/%s" % (FLAVOR_MAP[flavor], host, 'nosched',
                                        'nobatch', queue)
        else:
            # no queue
            log.info('Missing queue for {}/{}'.format(host, flavor))
            res = "%s://%s/%s/%s/%s" % (FLAVOR_MAP[flavor], host, 'nosched',
                                        'nobatch', 'noqueue')

        if flavor == 'ARC-CE':
            # arc2.farm.particle.cz:2811/nordugrid-Condor-grid
            c.add('org.sam.ARC-JobState-/lhcb/Role=production', hosts=(host,),
                  params={'args': {'--resource': '%s' % res}})
        else:
            # htcondor-ce
            c.add('org.sam.CONDOR-JobState-/lhcb/Role=production',
                  hosts=(host,),
                  params={'args': {'--resource': 'condor://%s' % host}})

    # WebDAV, GRIDFTP
    pat = re.compile(r"^\w+://.+:(\d*)(/.*)")
    defport = {'HTTP': 443, 'GRIDFTP': 2811, 'XROOTD': 1094}
    for service in services:
        host = service[0]
        flavor = service[1]
        if flavor not in ['HTTP', 'GRIDFTP', 'XROOTD']:
            continue
        endpoint = service[2]
        args_dict = dict()
        m = re.search(pat, endpoint)
        if (m):
            port = m.group(1)
            path = m.group(2)
            if (not port):
                port = defport[flavor]
            path = re.sub(r'/$', '', path)
            if not (re.search(r'/lhcb', path)):
                path = path + '/lhcb'

            args_dict['-P'] = port
            args_dict['-T'] = path
        args_dict['-H'] = host
        if ipv6:
            args_dict['-6'] = ''
        else:
            args_dict['-4'] = ''

        if flavor == 'HTTP':
            c.add("org.lhcb.SE-WebDAV-summary",
                hosts=(host,), params={'args': args_dict, '_tags': flavor})
        if flavor == 'GRIDFTP':
            c.add("org.lhcb.SE-GSIftp-summary",
                hosts=(host,), params={'args': args_dict, '_tags': flavor})
        if flavor == 'XROOTD':
            c.add("org.lhcb.SE-xrootd-summary",
                hosts=(host,), params={'args': args_dict, '_tags': flavor})

    c.serialize()

